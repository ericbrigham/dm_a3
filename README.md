# Instructions for Building and Running the Code

To build and run the code follow these instructions:  

- Clone the git repository so that you have all the files on your machine.   

- Open the terminal/command line.  

- Navigate to the file "dm_a3", this is where the data and the algorithms live.  

- Once here, navigate to "src." You can run the algorithms as follows:   

    - To run ID3, type in "python3 DTL.py ../data/titanic_training_csv.csv ../data/titanic_testing_csv.csv" to the terminal and hit enter.

 