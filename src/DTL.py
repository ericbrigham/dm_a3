import sys
import math
import pandas
import numpy 
from pprint import pprint

#Implements the main DTL algorithm.
# The class should somehow be encoded in the datapoints
#  structure. A reasonable approach would be to use
#  the feature name "class" to refer to it consistently

def DTL(datapoints, features, default_value):
    if datapoints.empty:
        return default_value
    elif len(datapoints['Survived'].unique()) == 1:
        #everyone survived or died
        return datapoints.iloc[0]['Survived']
        #get first cell value of survived colume
    elif len(features) == 0:
        return default_value
    else:
        best = Choose_Attribute(features, datapoints)
        #choosing best attribute
        tree = {best:{}}
        #creating a tree w/ best at root
        best_attributes = datapoints[best].unique()
        #finding all attributes from best column
        features.remove(best)
        for x in best_attributes:
            subset = datapoints.loc[datapoints[best] == x]
            #split data based on best attribute
            subtree = DTL(subset, features, Compute_Mode(datapoints))
            #recurse
            tree[best][x] = subtree
            #add children here
        return tree

#Returns the feature with the greatest information
# gain for the provided datapoints.
def Choose_Attribute(features, datapoints):
    gainzzz = []
    for column in features:
        gainzzz.append(Compute_Information(datapoints, column))
    maximum = numpy.argmax(numpy.asarray(gainzzz))
    return features[maximum]
    
#Returns a set of subsets of datapoints
# such that all points in a subset have
# the same value of the indicated feature.
def Make_Subsets(datapoints, feature):
    pass

#Computes the mode of the class of datapoints.
def Compute_Mode(datapoints):
    return datapoints.loc[:, 'Survived'].mode()[0]

#Computes the information entropy of datapoints w.r.t
# the class.
def Compute_Information(datapoints, column):
    #create subsets based on columns
    subsets = datapoints.groupby(column)
    num_entries = len(column)
    subsets_ae = subsets.agg({'Survived' : [Attribute_Entropy, lambda x: len(x)/num_entries]})['Survived']
    subsets_ae.columns = ["Entropy", "Density"]

    post_split_entropy = sum(subsets_ae["Entropy"] * subsets_ae["Density"])
    previous_entropy = Attribute_Entropy(datapoints["Survived"])
    return previous_entropy-post_split_entropy

def Attribute_Entropy(column):
    counts = dict()
    for x in column:
        if x in counts:
            counts[x] += 1
        else:
            counts[x] = 1
    
    num_entries = len(column)
    #density_array = []
    entropy = 0

    for k in counts.keys():
        density = counts[k] / num_entries
        #density_array.append(density)
        entropy += -density*math.log(density,2)
    return entropy 
    
def classify(tree, datapoint, Survived):
    #beginning of the tree
    root = list(tree.keys())[0]
    #check if the child is in "Survived" (0 or 1),  if it is, then it is a leaf and we return
    if tree[root][datapoint.iloc[0][root]] in Survived:
        return tree[root][datapoint.iloc[0][root]]
    else:
        #It wasnt a leaf, so we keep traversing to find a leaf
        return classify(tree[root][datapoint.iloc[0][root]], datapoint, Survived)

def accuracy(prediction, reality):
    count = 0
    for x in range(len(prediction)):
        if prediction[x] == reality[x]: 
            count += 1
    return count/len(prediction)

def main():
    #import the trainging and tesitng data
    training_data = pandas.read_csv(sys.argv[1])
    testing_data = pandas.read_csv(sys.argv[2])
    #isolate survived column from testing data into its own list
    survived_test = list(testing_data['Survived'])
    #get two unique survived labels
    Survived = training_data['Survived'].unique()
    #remove the 'Survived' Column from the testing data
    testing_data = testing_data.drop(['Survived'], axis =1)
    features = list(testing_data.columns)
    default_value = Compute_Mode(training_data)
    tree = DTL(training_data, features, default_value)
    pprint(tree)

    predictions = []
    #loop through the rows and predict
    for index, row in testing_data.iterrows():
        predictions.append(classify(tree, testing_data.iloc[[index]], Survived))
    
    print("Accuracy", accuracy(predictions, survived_test))

if __name__ == "__main__":
    main()