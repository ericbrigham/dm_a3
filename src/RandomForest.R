options(repos='http://cran.rstudio.org')
have.packages <- installed.packages()
cran.packages <- c('devtools','plotrix','randomForest','tree')
to.install <- setdiff(cran.packages, have.packages[,1])
if(length(to.install)>0) install.packages(to.install)
library(dplyr)

library(devtools)
if(!('reprtree' %in% installed.packages())){
  install_github('araastat/reprtree')
}
for(p in c(cran.packages, 'reprtree')) eval(substitute(library(pkg), list(pkg=p)))

library(randomForest)
library(reprtree)

training_data$X1 <- NULL
training_data$X1_2 <- NULL
training_data$X1_1 <- NULL
training_data$Age <- NULL
training_data$Fare <- NULL

data_fix=training_data %>% mutate_if(is.character, as.factor)
data_fix$Survived <- as.character(data_fix$Survived)
data_fix$Survived <- as.factor(data_fix$Survived)

model <- randomForest(Survived ~ ., data=data_fix, importance=TRUE, ntree=500, mtry = 2, do.trace=100)

conf <- model$confusion

reprtree:::plot.getTree(model)