#Data Mining Assignment 3
import sys
sys.path.append("C:/Users/Hunter/Anaconda3/Lib/site-packages")      #NOTE: For everyone else, you'll have to change this to wherever pip installs packages
import anytree
import math
#from anytree import anynode



def DTL(points, features, default_Value):   #points are a list, features are what we're trying to predict/find out about and have some value.
    if points == None:                      #Used to stop algorithm when we're at the bottom of the tree (no points left) when we're recursing
        return default_Value
    else:
        #If there are no features, return a guess/most common datapoint. 
        if features == None:
            return ComputeMode(points)
        #NOTE TO SELF: Ask if there's any problem with putting the features == None check before the all point check.
        else:
            #If all the points are the same, return that value
            if checkAll(points) == True:
                return points[0]    #Technically, any value would be fine. Just using the first one to avoid index issues.
            else:
                featureclone = feature.copy()                                 #Makes a copy of feature list so we can remove things from it without potientially damaging the original
                bestChoice = ChooseAttributes(points, features)     #Best will be our root node
                #Need a way to make a tree. 
                #Using anytree
                #testing = AnyNode(id="root")    #Just testing to see everything imported correctly.
                #Notes about AnyNode:
                #API: A generic tree node with any kwargs.
                #Has a parent value. If empty, its assumed to be the root. Id is the node's name. 
                bestChoiceNode = AnyNode(id="root", nodeFeature = bestChoice)   #nodeFeature is something I made to hold what feature is associated with the node
                #because I can't (for now at least) figure out any other way of letting the node hold the feature.
                #Questions for later: How do we remove bestCHoice from the list of data? What happens if there are duplicate values of bestChoice (i.e. 3 data points with the same information)?
                #UPDATE We're removing classes not indivdual points. As such, there should only be unique classes.
                
                
                
            

    pass

#Walkthrough of Entropy. Find feature with
#Need to find entropy of whole system.
#Find entropy of individual features. 
#We split on feature with highest information gain (the feature that reduces entropy the most)

def calculateProportion(moreData):   #Helper function. Computes the proportion of each class in a set of data. 
    howManyTimes = {}                     #Makes a dictionary.
    for q in moreData:
        if q not in howManyTimes:       #Not in returns true if something is not in a dictionary. 
            howManyTimes[q] = 1         #Since there's at least one of the class, we've seen it once.
        else:
            times = howManyTimes[q]   #Updates our count
            times += 1
            howManyTimes[q] = times
    proportion = {}
    for k,v in howManyTimes.items():   #Grabs the key and its value.
        proportion[k] = v/len(moreData)     #Should record the proportion of each class in data
    return proportion
    #How much data should be len(moreData)
    
def logFixer(value):            #Helper function that helps absolve a special case detailed below in entropy. 
    if value ==0:
        return 0
    return (math.log(value)/math.log(2.0))

def findEntropy(someData):
    #NOTE Entropy MUST have at least two numbers. Having only one class causes errors!
    #Needs a proportion of the classes in a data set in order to work. Use calculateProportion to get that proportion.
    #Because calculateProportion returns a dictionary, need to modifiy entropy to work with it.
    #Used for testing
    #print(someData)
    #WARNING: log2(0) is actually -Infinity. We however, treat log2(0) as just zero. Need a special case for that.
    #entropy = - sum([i * math.log(i)/ math.log(2.0) for t, i in someData.items()]) #Backup working one-line version.
    #Can replace math.log(i)/ math.log(2.0) with a function
    entropy = - sum([i * logFixer(i) for t, i in someData.items()]) 
    #The amount of classes we have determines the entropy range. 2 Classes - 0 to 1. 4 Classes 0 to 4
    #To normalize data, we divide by 
    finalEntropy = entropy/math.log2(len(someData))                    
    return finalEntropy


def ChooseAttributes(theFeatures, theData):
    pass

def ComputeMode(datapoints):
    pass

def MakeSubsets(pointsofdata, feature):
    pass

def ComputeInformation(pointsData):
    pass


def checkAll(pointList):    #Helper function that checks if a list is made up of the same points
    checkValue = pointList[0]
    for stuff in pointList:
        if checkValue != stuff:     #If anything isn't the same. 
            return False
    return True


    #Testing#
#CheckAll Test#
fruits = ['orange', 'apple', 'pear', 'banana', 'kiwi', 'apple', 'banana']
numbers = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ]
Nikes = [1, 1, 1, 1, 'walrus', 1, 1,  1, 1,  'You are a Pirate', 1, 1,  1, 1,  1, 1, 'Quake']
letters = ['Baka', 'Baka', 'Baka', 'Baka', 'Baka', 'Baka', 'Baka', 'Baka', 'Baka', 'Baka']
Krool = [1, 1, 12, 1, 'walrus', 61, 1,  13, 1,  'You are a Pirate', 1, 1,  1, 1,  1, 1, 'Quake', 'King', 'King', 'King', 12, 'walrus', 1098492, 'Not Today']
chaos = [2, 'NO', 999999999, 'Ocelot', 'Moose', 1234567890]
##print(checkAll(fruits))
##print(checkAll(numbers))
##print(checkAll(Nikes))
##print(checkAll(letters))
#Passed.

#calculateProportion Test#
##print(calculateProportion(Krool))
##print(calculateProportion(letters))  #Should be 1 (100%) since they're all the same
##print(calculateProportion(fruits))
##print(calculateProportion(Nikes))
##print(calculateProportion(numbers))
#Passed
#Entropy Test#                   #The more homogenous the values are the close the entropy is to zero.
letterPro = calculateProportion(letters)
fruitsPro = calculateProportion(fruits)
NikesPro = calculateProportion(Nikes)
numbersPro = calculateProportion(numbers)
KroolPro = calculateProportion(Krool)
chaosPro = calculateProportion(chaos)
#print("letter")
#print(findEntropy(letterPro))     #Should be 0 as all the values are the same and hence less disorder. UPDATE Entropy can't work on single class datasets. 
print("Fruits")
print(findEntropy(fruitsPro))
print("Nikes")
print(findEntropy(NikesPro))
#print("Numbers")
#print(findEntropy(numbersPro))
print("Krool")
print(findEntropy(KroolPro))
print("chaos")
print(findEntropy(chaosPro))